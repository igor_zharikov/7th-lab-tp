//
//  ViewController.m
//  7lab_1st_task
//
//  Created by Admin on 26.04.16.
//  Copyright © 2016 Admin. All rights reserved.
//

#import "ViewController.h"

@interface ViewController ()
@property (weak, nonatomic) IBOutlet UIImageView *canvas;
@property CGPoint lastPoint;
@property (weak, nonatomic) IBOutlet UITextField *widthView;
@property (weak, nonatomic) IBOutlet UITextField *fileName;
@property (weak, nonatomic) IBOutlet UIPickerView *chooseColor;
@property NSArray *pickerData;
@property NSArray *reds;
@property NSArray *green;
@property NSArray *blue;
@property int selectedPicker;
@end

@implementation ViewController

- (IBAction)saveToFile:(id)sender {
    NSString *fileName = [_fileName text];
    // Create path.
    NSArray *paths = NSSearchPathForDirectoriesInDomains(NSDocumentDirectory, NSUserDomainMask, YES);
    NSString *filePath = [[paths objectAtIndex:0] stringByAppendingPathComponent:fileName];
    NSLog(@"%@", filePath);
    // Save image.
    [UIImagePNGRepresentation(_canvas.image) writeToFile:filePath atomically:YES];
}

- (void)viewDidLoad {
    [super viewDidLoad];
    _pickerData = @[@"red", @"green", @"blue"];
    _selectedPicker = 0;
    _reds = @[ @1.0, @0.0, @0.0];
    _green = @[@0.0, @1.0, @0.0];
    _blue = @[@0.0, @0.0, @1.0];
    self.chooseColor.dataSource = self;
    self.chooseColor.delegate = self;
    // Do any additional setup after loading the view, typically from a nib.
}

- (void)didReceiveMemoryWarning {
    [super didReceiveMemoryWarning];
    // Dispose of any resources that can be recreated.
}

// The number of columns of data
- (int)numberOfComponentsInPickerView:(UIPickerView *)pickerView
{
    return 1;
}

// The number of rows of data
- (int)pickerView:(UIPickerView *)pickerView numberOfRowsInComponent:(NSInteger)component
{
    return _pickerData.count;
}

// The data to return for the row and component (column) that's being passed in
- (NSString*)pickerView:(UIPickerView *)pickerView titleForRow:(NSInteger)row forComponent:(NSInteger)component
{
    return _pickerData[row];
}
- (void)touchesBegan:(NSSet *)touches withEvent:(UIEvent *)event{
    UITouch *touch = [touches anyObject];
    
    [self setLastPoint:[touch locationInView:self.canvas]];
}
// Catpure the picker view selection
- (void)pickerView:(UIPickerView *)pickerView didSelectRow:(NSInteger)row inComponent:(NSInteger)component
{
    // This method is triggered whenever the user makes a change to the picker selection.
    // The parameter named row and component represents what was selected.
    _selectedPicker = row;
}
- (void)touchesMoved:(NSSet *)touches withEvent:(UIEvent *)event{
    
    double width = [[_widthView text] doubleValue];
    double red = [_reds[_selectedPicker] doubleValue];
    double green = [_green[_selectedPicker] doubleValue];
    double blue = [_blue[_selectedPicker] doubleValue];
    UITouch *touch = [touches anyObject];
    
    CGPoint currentPoint = [touch locationInView:self.canvas];

    UIGraphicsBeginImageContext(self.canvas.frame.size);
    
    CGRect drawRect = CGRectMake(0.0f, 0.0f, self.canvas.frame.size.width,
                                 
                                 self.canvas.frame.size.height);
    
    [[[self canvas] image] drawInRect:drawRect];
    
    CGContextSetLineCap(UIGraphicsGetCurrentContext(), kCGLineCapRound);
    
    CGContextSetLineWidth(UIGraphicsGetCurrentContext(), width);
    
    CGContextSetRGBStrokeColor(UIGraphicsGetCurrentContext(), red, green, blue,
                               
                               1.0f);
    
    CGContextBeginPath(UIGraphicsGetCurrentContext());
    
    CGContextMoveToPoint(UIGraphicsGetCurrentContext(), _lastPoint.x,
                         
                         _lastPoint.y);
    
    CGContextAddLineToPoint(UIGraphicsGetCurrentContext(), currentPoint.x,currentPoint.y);
    
    CGContextStrokePath(UIGraphicsGetCurrentContext());
    
    [[self canvas] setImage:UIGraphicsGetImageFromCurrentImageContext()];
    
    UIGraphicsEndImageContext();
    
    _lastPoint = currentPoint;
}

@end
